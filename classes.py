

#This file is meant to hold the classes that will be used to gather
#the data from the csv files and hold it that so the sentiment
#analysis algorithm can then do things with it, and eventually
#organize it all.
from textblob import TextBlob

class timeFrame:

    #CI:hour and riseFall are both defined, hour as a dateTime object
    #and riseFall as a float 
    
    #PRE:constructor
    #POST:this constructor takes a time(hour) and riseFall which will
    #be a float representing the rise or fall of the price of the
    #cryptocurrency this hour
    def __init__(self,riseFall):
        #Represents the hour of the day
        if riseFall[0] == '-':
            #ASSERT:is a negative number
            
            #ASSERT:we want to use the decimal value to try and predict exact price 
            self.riseFallExact = riseFall[0:5]
            
            #ASSERT:we just want to try and predict if it rises or falls
            self.riseFall = '-1'
        else:
            #ASSERT:is a positive number
            
            self.riseFallExact = riseFall[0:4]
            
            self.riseFall = '1'
        
        self.twitterSentiment = 0
        self.redditSentiment = 0
        self.sentiment = 0
        self.numThings = 0
        self.twitterPurePos = 0
        self.twitterPureNeg = 0
        self.redditPurePos = 0
        self.redditPureNeg = 0
        #tweets will be a list containing tweet objects
        self.tweets = []
        #posts will be a list containing redditPost objects
        self.posts = []


    #PRE:tweetObj is a tweet object
    #POST:appends the tweet object to the back of the tweets list
    def addTweet(self,tweetObj):
        self.tweets.append(tweetObj)
        self.numThings = self.numThings + 1

    #PRE:redditObj is a reddit object
    #POST:appends the reddit object to the back of the reddit list
    def addReddit(self,redditObj):
        self.posts.append(redditObj)
        self.numThings = self.numThings + 1

    #PRE:obj satisfies the CI
    #POST returns the tweets list
    def getTweets(self):
        return self.tweets

    
    #PRE:obj satisfies the CI
    #POST returns the reddit posts list
    def getPosts(self):
        return self.posts

    #PRE:obj satisfies the CI
    #POST: returns the number of posts and tweets in obj
    def getNumThings(self):
        return self.numThings

    #PRE:object satisfies the CI
    #POST:returns the overall sentiment of this timeframe
    def getSentiment(self):
        return self.sentiment

    
    #PRE:object satisfies the CI
    #POST:returns the overall twitter sentiment of this timeframe
    def getTwitterSentiment(self):
        #print("overal twit sentiment")
        #print(self.twitterSentiment)
        return self.twitterSentiment
    

    #PRE:object satisfies the CI
    #POST:returns the purePos twitter sentiment of this timeframe
    def getTwitterPurePos(self):
        #print("pure pos twit")
        #print(self.twitterPurePos)
        return self.twitterPurePos
    
    #PRE:object satisfies the CI
    #POST:returns the pureNeg twitter sentiment of this timeframe
    def getTwitterPureNeg(self):
        #print("pure neg twit")
        #print(self.twitterPureNeg)
        return self.twitterPureNeg
    
    #PRE:object satisfies the CI
    #POST:returns the overall reddit sentiment of this timeframe
    def getRedditSentiment(self):
        return self.redditSentiment

    #PRE:object satisfies the CI
    #POST:returns the purePos reddit sentiment of this timeframe
    def getBothPurePos(self):
        return self.redditPurePos + self.twitterPurePos
    #PRE:object satisfies the CI
    #POST:returns the pureNeg reddit sentiment of this timeframe
    def getBothPureNeg(self):
        return self.redditPureNeg + self.twitterPureNeg


    #PRE:object satisfies the CI
    #POST:returns the riseFall member data
    def getRiseFall(self):
        return self.riseFall
    
    #PRE:object satisfies the CI
    #POST:returns the riseFall member data
    def getRiseFallExact(self):
        return self.riseFallExact


    #PRE:object satisfies the CI
    #POST:runs through all data held in tweets list and
    #gets the average sentiment for this hour and puts it into the
    #sentiment member data object.
    def findTwitterSentiment(self):
        for tweet in self.tweets:
            numRTS = tweet.getRetweets()
            analysis = TextBlob(tweet.getText())
            tweetSentiment = 0
            if(analysis.sentiment.polarity > 0):
                #ASSERT:the polarity was positive
                self.twitterPurePos = self.twitterPurePos + analysis.sentiment.polarity
                #print("This is twitter pure pos")
                #print(self.twitterPurePos)
                if(int(numRTS) > 0):
                    #ASSERT:This tweet was retweeted giving it significance
                    tweetSentiment = 2
                else:
                    #ASSERT:was not retweeted so less significance
                    tweetSentiment = 1
            elif(analysis.sentiment.polarity < 0):
                #ASSERT:tweet polarity was negative
                self.twitterPureNeg = self.twitterPureNeg + analysis.sentiment.polarity
                #print("This is twitter neg")
                #print(self.twitterPureNeg)
                if(int(numRTS) > 0):
                    #ASSERT:tweet was retweeted giving it significance
                    tweetSentiment = -2
                else:
                    #ASSERT:tweet was not retweeted so less significance
                    tweetSentiment = -1
            else:
                #ASSERT:tweet polarity was neutral
                tweetSentiment = 0
            #ASSERT:tweet sentiment has been updated correctly
            self.twitterSentiment = self.twitterSentiment + tweetSentiment
            #print("This is twitter sentiment here")
            #print(self.twitterSentiment)
            


            
    #PRE:object satisfies the CI
    #POST:goes through all of the reddit objects for this time frame
    #and calculates the sentiment and adds it up
    def findRedditSentiment(self):
        for post in self.posts:
            numLikes = post.getLikes()
            analysis = TextBlob(post.getText())
            postSentiment = 0
            if(analysis.sentiment.polarity > 0):
                #ASSERT:the post was deemed positive
                self.redditPurePos = self.redditPurePos + analysis.sentiment.polarity
                if(int(numLikes) > 0):
                    #ASSERT:This post or comment was liked
                    postSentiment = 2
                else:
                    #ASSERT:This post or comment was not liked
                    postSentiment = 1
            elif(analysis.sentiment.polarity < 0):
                self.redditPureNeg = self.redditPureNeg + analysis.sentiment.polarity
                if(int(numLikes) > 0):
                   #ASSERT:this post or comment was liked
                    postSentiment = -2
                else:
                    #ASSERT:This post or comment was not liked
                    postSentiment = -1
            else:
                #ASSERT:post was deemed neutral
                postSentiment = 0
            #ASSERT:post sentiment has been updated correctly
            self.redditSentiment = self.redditSentiment + postSentiment
        

    #PRE:object satisfies the CI
    #POST:gets the reddit sentiment and twitter sentiment and adds them
    def findSentiment(self):
        self.findRedditSentiment()
        self.findTwitterSentiment()
        self.sentiment = self.twitterSentiment + self.redditSentiment
        


class tweet:
    #CI:Text should always contain some string and retweet should be a
    #nonzero integer

    #PRE:Constructor
    #POST:Creates an object containing a string called text and an
    #integer representing the number of retweets this text got.
    def __init__(self,text,retweets):
        self.text = text
        self.retweets = retweets

    #PRE:object satisfies the CI
    #POST:returns the text member data
    def getText(self):
        return self.text

    #PRE:object satisfies the CI
    #POST:returns the retweets member data
    def getRetweets(self):
        return self.retweets


class redditPosts:
    #CI:Text member data object should always contain a string and
    #likes will be any integer(represents upVotes or downvotes)

    #PRE:Constructor
    #POST:Creates an object containing a string called text and an
    #integer containing the number of likes this post gets
    def __init__(self,text,likes):
        self.text = text
        self.likes = likes

    #PRE:object satisfies the CI
    #POST:returns the text of this object
    def getText(self):
        return self.text

    #PRE:Object satisfies the CI
    #POST:returns the likes of this object
    def getLikes(self):
        return self.likes
    

        
