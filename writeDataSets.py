from classes import timeFrame, tweet, redditPosts
import csv
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import numpy as numpy

RISEFALLINDICE = 3
ENDTIME = 13
TWEETTEXT = 0
TIME = 1
REDDITTEXT = 0
RETWEETS = 2
LIKES = 2
ENDDATE = 10
STARTTIME = 11

#create empty dictionary for all times
timeDict = {}

#read in ethPrices.csv
timePrices = []

count = 0

with open("ethPrices.csv") as csvFile:
    reader = csv.reader(csvFile, delimiter = ",")
    #each row is a list in itself
    for row in reader:
        timePrices.append(row)
        newTimeFrame = timeFrame(timePrices[count][RISEFALLINDICE])
        timeDict[(timePrices[count][0])[0:ENDTIME]] = newTimeFrame
        count +=1
#ASSERT:all times that were present in the ethPrices csv have had
#timeFrame objects created for them initialzed with their risefall indice

with open("allTweets.csv") as csvFile:
    reader = csv.reader(csvFile,delimiter = ",")
    tweetCount = 0
    anotherCounter = 0
    for row in reader:
        if(tweetCount != 0):
            tweetObj = tweet(row[TWEETTEXT], row[RETWEETS])
            aTime = row[TIME]
            dictTime = row[TIME][0:ENDDATE] + "T" + row[TIME][STARTTIME:ENDTIME]
            if(timeDict.get(dictTime) != None):
                #ASSERT:this time has a price rise fall and is valid
                timeDict[dictTime].addTweet(tweetObj)
                anotherCounter += 1
        tweetCount += 1
#print(anotherCounter)
#print(tweetCount)

with open("redditStuff.csv") as csvFile:
    reader = csv.reader(csvFile, delimiter = ",")
    redditCounter = 0
    for row in reader:
        redditObj = redditPosts(row[REDDITTEXT],row[LIKES])
        aTime = row[TIME]
        dictTime = aTime[0:ENDDATE] + "T" + aTime[STARTTIME:ENDTIME]
        if(timeDict.get(dictTime) != None):
            #ASSERT:is a valid time
            timeDict[dictTime].addReddit(redditObj)
            redditCounter += 1
#print(redditCounter)



#ASSERT:All data has been added to the correct timeFrame object
aList = []
for aKey in timeDict:
    if(timeDict[aKey].getNumThings() != 0):
        aList.append(timeDict[aKey])
        #ASSERT:all things in dictionary have been appended to the list


for aThing in aList:
    #finds all sentiment info for every timeframe
    aThing.findSentiment()

##with open("bothDataExact.csv",'w') as csvfile:
 ##   filewriter = csv.writer(csvfile,delimiter=',')
  ##  for aThing in aList:
    ##    filewriter.writerow([aThing.getSentiment(),aThing.getBothPurePos(),aThing.getBothPureNeg(),aThing.getRiseFallExact()])

        
with open("bothDataRiseFall.csv",'w') as csvfile:
    filewriter = csv.writer(csvfile,delimiter=',')
    for aThing in aList:
        filewriter.writerow([aThing.getSentiment(),aThing.getBothPurePos(),aThing.getBothPureNeg(),aThing.getRiseFall()])

##with open("noRedditRiseFallExact.csv",'w') as csvfile:
  ##  filewriter = csv.writer(csvfile,delimiter=',')
  ##  for aThing in aList:
   ##     if((aThing.getTwitterSentiment() != 0) and (aThing.getTwitterPurePos() !=0) and (aThing.getTwitterPureNeg() != 0)):
            #Throws out hours that only have reddit data
     ##       filewriter.writerow([aThing.getTwitterSentiment(),aThing.getTwitterPurePos(),aThing.getTwitterPureNeg(),aThing.getRiseFallExact()])

        
with open("noRedditRiseFall.csv",'w') as csvfile:
    filewriter = csv.writer(csvfile,delimiter=',')
    for aThing in aList:
        #Throws out hours that only have reddit data
        if((aThing.getTwitterSentiment() != 0) and (aThing.getTwitterPurePos() !=0) and (aThing.getTwitterPureNeg() != 0)):
            filewriter.writerow([aThing.getTwitterSentiment(),aThing.getTwitterPurePos(),aThing.getTwitterPureNeg(),aThing.getRiseFall()])
            
