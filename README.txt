Author: Taylor Yarn

**All CSV files simply contain data that was either gathered week to
  week for each type of information needed for this research, or was
  compiled from the data gathered as input to the Machine Learning
  Algorithm. None of the code needs to be compiled


gatherTweets:
This file is supposed to be run as a script in the R interface
environment. It contains lines of code that loads the rtweet library,
a function from rtweet to gather all of the tweets for a certain
amount of dates, these dates would change week to week as I gathered
new tweets every week, and a function to write this data to a csvfile
and save the csv file. I chose to get the tweets week by week because
the Twitter API does not allow you to access tweets more than a week
old without a paid account.


cleanTweets.py:
This Python script takes a csv file that was created by the previously
mentioned r script, gatherTweets, and cleans the file, i.e gets rid of
all of the columns that were returned with each tweet object that I
did not need for my research.

getReddit.py:
This python script is what gathers data from reddit week by week. It
contains my Reddit authorization information so that when I access
Reddit through this script it will allow me to gather data. It also
writes all of the information that I need from Reddit, such as the
time the post or comment was put on reddit, the amount of upvotes it
has, and the post or comment itself, into a csv file.

getPrices.py:
This python script is what I used to gather the price of Ethereum
every hour for a certain week. I would change the data parameter on
the URL Request every week when I did run the script. Each week the
rise or fall data for Ethereum for each hour that week was written into a csv file so that I could use the
rise/fall data for each timeframe at a different time.


helper.py:
This Python file contains a function to return the dateTime of a
reddit submission and was used in the file getReddit.py



classes.py:
classes.py is a file that I created to hold the classes that would
allow me to organize the data I was gathering into objects. I chose to
gather the data into objects because I believed that it would allow me
to organize it better and it also allowed me to write one function
that could be used to essentially find the sentiment and gather the
data I needed to put into the csv file for XGBoost into an
object. This file is well documented and is not mean to be run as an
executable by Python3


writeDataSets.py:
This Python script, when run, will create four new csv files from the
data you have given it in other csv files. These four new csv files
represent the data that are supposed to be given to XGBoost to see how
well it could predict the rise and fall of Ethereum.


bothRiseFall.py:
This is the Python Script that takes data from bothDataRiseFall.csv
and uses it as input to XGBoost and gets out the prediction rates that
XGBoost got with the data that uses both Reddit and Twitter data.

noRedditRiseFall.py:
This is the Python script that takes data from noRedditRiseFall.csv
and uses it as input to XGBoost and gets out the prediction rates the
XGBoost got with data just from Twitter.

//As a note yes I know I could do bothRiseFall and noRedditRiseFall in
the same file but I did them separately for my own organizational
sanity

