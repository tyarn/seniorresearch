
import praw
import datetime
#PRE:aThing is a reddit object that we have gotten, either a
#submission or a comment
#POST:returns the datetime of the comment or submission when it was
#created in string form
def getDateTime(aThing):
        time = aThing.created
        #getting the creation time of the object
        #now returning the datetime as a string
        return str(datetime.datetime.fromtimestamp(time))
