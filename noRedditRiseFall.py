from classes import timeFrame, tweet, redditPosts
import csv
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import numpy as numpy
import pandas as pandas

RISEFALLINDICE = 3
ENDTIME = 13
TWEETTEXT = 0
TIME = 1
REDDITTEXT = 0
RETWEETS = 2
LIKES = 2
ENDDATE = 10
STARTTIME = 11

data = pandas.read_csv('noRedditRiseFall.csv',header=None)
dataset = data.values
#split into x and y data

xList = dataset[:,0:3]
yList = dataset[:,3]


largest = 0
average = 0
smallest = -1

test_size = 0.33

for i in range(1000):
    x_train, x_test, y_train, y_test = train_test_split(xList,yList,test_size = test_size, random_state = i)

    model = XGBClassifier()
    model.fit(x_train,y_train)
    y_pred = model.predict(x_test)
    predictions = [round(value) for value in y_pred]
    accuracy = accuracy_score(y_test, predictions)
    if(accuracy > largest):
        #ASSERT:found a better accuracy
        largest = accuracy
    if(smallest == -1):
        #first iteration
        smallest = accuracy
    else:
        #ASSERT:already have a smallest
        if(smallest > accuracy):
            #ASSERT: found new smallest
            smallest = accuracy
    average = average + accuracy



print("Largest: %.2f%%" % (largest * 100.0))
print("Smallest: %.2f%%" % (smallest * 100.0))
average = average/1000
print("Average: %.2f%%" % (average * 100.0))
